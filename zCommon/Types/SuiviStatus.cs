﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IC.Common
{
	/// <summary>
	/// Gère la conversion Nom - Id des statuts de suivi
	/// </summary>
	public enum SuiviStatus
	{
		/// <summary>
		/// Statut d'erreur
		/// </summary>
		Error = 0,
		/// <summary>
		/// Statut de reussite
		/// </summary>
		Success = 1,
		/// <summary>
		/// Statut NonFait
		/// </summary>
		NotRun = 2,
		/// <summary>
		/// Statut d'avertissement
		/// </summary>
		Warning = 3
	}
}
