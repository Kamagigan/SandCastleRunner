﻿namespace IC.Common
{
	/// <summary>
	/// Enumerre les types de message pour le log
	/// </summary>
	public enum MessageType
	{
		/// <summary>
		/// Type de message destiné à noté ce qui se passe dans les classes et activités utilisant le log 
		/// </summary>
		Trace,
		/// <summary>
		/// Type de message destiné à noté les erreurs rencontré par les classes et activités utilisant le log
		/// </summary>
		Error,
        /// <summary>
        /// Type de message destiné à noté les exceptions rencontré par les classes et activités utilisant le log
        /// </summary>
        Exception,
		/// <summary>
		/// Type de mssage destiné à noté une anormzalie rencontré par les classes et activités utilisant le log
		/// </summary>
		Warning,
		/// <summary>
		/// Type de message par défaut destiné à noté tous les messages qui ne rentrent pas dans les autres cas 
		/// </summary>
		Default,
	}

	/// <summary>
	/// Enumerre les niveaux d'importance pour le log
	/// </summary>
	public enum MessageLevel
	{
		/// <summary>
		/// Iportance Capitale
		/// </summary>
		Critical,
		/// <summary>
		/// Haute Importance
		/// </summary>
		High,
		/// <summary>
		/// Moindre Importance
		/// </summary>
		Low,
		/// <summary>
		/// Importance normale, par defaut
		/// </summary>
		Default
	}
}
