﻿namespace IC.Common
{   
	/// <summary>
	/// Liste des différents types de paramétrage
	/// </summary>
    public struct ParamType
	{
		/// <summary>
		/// ParamType : EXCLUDE_BIN
		/// </summary>
		public const string EXCLUDE_BIN = "EXCLUDE_BIN";
		/// <summary>
		/// ParamType : INCLUDE_BIN
		/// </summary>
		public const string INCLUDE_BIN = "INCLUDE_BIN";
		/// <summary>
		/// ParamType : BUILDER_FRAMEWORK_VERSION
		/// </summary>
		public const string BUILDER_FRAMEWORK_VERSION = "BUILDER_FWK_VERSION";
		/// <summary>
		/// ParamType : MISE_A_JOUR_MS_DESCRIPTION_FORCE
		/// </summary>
		public const string MISE_A_JOUR_MS_DESCRIPTION_FORCE = "MAJ_MS_DESCRIPTION";
	}
}
