﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IC.Common
{
	/// <summary>
	/// Gère la conversion Nom - Id des types de regroupement de suivi
	/// </summary>
	public enum SuiviRegroupmentType
	{
		/// <summary>
		/// Contient le regroupement des étapes de Compilation
		/// </summary>
		Compilation = 1,
		/// <summary>
		/// Contient le regroupement des étapes de Tests Unitaires
		/// </summary>
		UnitTests = 2,
		/// <summary>
		/// Contient le regroupement des étapes de Deploiement
		/// </summary>
		Deployment = 3,
		/// <summary>
		/// Contient le regroupement des étapes de Tests d'Integration
		/// </summary>
		IntegrationTests = 4,
		/// <summary>
		/// Contient le regroupement des étapes de Tests de Qualité (Code, SQL, JS, Duplication)
		/// </summary>
		Quality = 5,
		/// <summary>
		/// Contient le regroupement des étapes de Documentation et de Métrique
		/// </summary>
		Documentation = 6,
		/// <summary>
		/// Contient le regroupement des étapes de Packaging et de Publication
		/// </summary>
		Packaging = 7
	};
}
