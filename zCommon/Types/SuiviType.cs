﻿namespace IC.Common
{
	/// <summary>
	/// Gère la conversion Nom - Id des types de suivi de la table IC_PROJET_SUIVI_TYPE
	/// </summary>
	public enum SuiviType
	{
		/// <summary>
		/// Etape Compilation
		/// </summary>
		Compilation = 0,
		/// <summary>
		/// Etape Couverture des tests Unitaires
		/// </summary>
		UnitTestsCover = 1,
		/// <summary>
		/// Etape de Couverture des tests d'integration
		/// </summary>
		IntegrationTestsCover = 2,
		/// <summary>
		/// Etape de Metriques de code
		/// </summary>
		CodeMetrics = 3,
		/// <summary>
		/// Etape de Qualité(PAQL) de Code .Net
		/// </summary>
		CodeAnalysis = 4,
		/// <summary>
		/// Etape de Documentation du code
		/// </summary>
		Documentation = 5,
		/// <summary>
		/// Etape de Qualité(PAQL) SQL
		/// </summary>
		SqlAnalysis = 6,
		/// <summary>
		/// Etape de recherche de Duplication/clones du code 
		/// </summary>
		CodeDuplication = 7,
		/// <summary>
		/// Etape de Qualité(PAQL) Javascript
		/// </summary>
		JsAnalysis = 8,
		/// <summary>
		/// Etape de Tests Unitaires
		/// </summary>
		UnitTests = 9,
		/// <summary>
		/// Etape de Tests d'integration 
		/// </summary>
		IntegrationTests = 10,
		/// <summary>
		/// Etape Déploiement
		/// </summary>
		Deployment = 11,
		/// <summary>
		/// Etape AspWebW3C
		/// </summary>
		AspWebW3CAnalysis = 12,
		/// <summary>
		/// Etape Packaging
		/// </summary>
		Packaging = 13,
        /// <summary>
        /// Etape Publication documentation vers Sharepoint
		/// </summary>
		PublicationDoc = 14
	};
}
