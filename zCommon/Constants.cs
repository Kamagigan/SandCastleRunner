﻿using System;

namespace IC.Common
{
	/// <summary>
	/// Fichier definissant les constantes de configuration de IC_Common
	/// </summary>
	public static class Constants
	{
		/// <summary>
		/// Nom du fichier de ressources
		/// </summary>
		public static readonly string ResourceFile = "Messages.resx";
	}
}
