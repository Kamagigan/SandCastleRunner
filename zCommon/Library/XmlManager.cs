﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Net;

namespace IC.Common
{
	/// <summary>
	/// Fournit les accès Xml pour les processus de l'intégration Continue
	/// </summary>
	public class XmlManager : IDisposable
	{
		private XDocument Doc;
		public string XmlFilePath { get; set; }
		private XmlNamespaceManager NamespacesManager = null;
		public XNode currentNode = null;

        private static XmlManager xmlManager;

		/// <summary>
		/// Constructeur de classe
		/// </summary>
		/// <param name="xmlFilePath">Chemin du fichier Xml à utiliser</param>
		/// <param name="tryToOpen">Si le constructeur doit essayer d'ouvrir le fichier Xml</param>
		private XmlManager()
		{
			
		}

        /// <summary>
        /// Recupère l'instance de XmlManager
        /// </summary>
        /// <returns></returns>
        public static XmlManager getInstance()
        {
            if (xmlManager == null)
                xmlManager = new XmlManager();

            return xmlManager;
        }

		/// <summary>
		/// Ouvre un fichier Xml et prépare la classe à l'utiliser
		/// </summary>
		/// <param name="xmlFilePath">Chemin Du fichier Xml à ouvrir</param>
		/// <returns>Booléen de reussite(true) ou d'echec(false) d'ouverture du fichier</returns>
		public bool Open(string xmlFilePath)
		{
			try
			{
				if (File.Exists(xmlFilePath))
				{
					this.Doc = XDocument.Load(xmlFilePath);
					return true;
				}
				else
				{
					throw new IOException("le fichier xml spécifié n'existe pas : " + xmlFilePath);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        /// <summary>
        /// Charge le document passé en paramètre
        /// </summary>
        /// <param name="xmlFilePath"></param>
        /// <param name="tryToOpen"></param>
        public void LoadDocument(string xmlFilePath, bool tryToOpen)
        {
            this.Doc = new XDocument();
            this.XmlFilePath = xmlFilePath;
            this.NamespacesManager = new XmlNamespaceManager(new NameTable());

            if (tryToOpen)
            {
                try
                {
                    this.Open(xmlFilePath);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

		/// <summary>
		/// Sauvegarde le fichier modifié
		/// </summary>
		/// <param name="path">override du chemin du fichier</param>
		public void Save(string path = null)
		{
			if (String.IsNullOrEmpty(path))
			{
				this.Doc.Save(this.XmlFilePath);
			}
			else
			{
				this.Doc.Save(path);
			}
		}

		/// <summary>
		/// valide le fichier Xml selon un schema Xml
		/// </summary>
		/// <param name="schemaPath">Chemin du schema Xml à utiliser pour valider le fichier Xml</param>
		/// <returns>Booléen de Reussite(true) ou echec (false) de la validation Xml</returns>
		/// <remarks>En cas d'echec de la validation Xml, consulter le fichier de log (type:error et importance:High pour connaitre les raisons de l'échec</remarks>
		public bool ValidateDocument(string schemaPath)
		{
			bool result = true;

			try
			{
				//Recuperation fichier schema xsd sur sharepoint
				var webClient = new WebClient();
				webClient.Credentials = CredentialCache.DefaultCredentials;
				Stream streamSchema = webClient.OpenRead(schemaPath);
				XmlReader xmlreader = XmlReader.Create(streamSchema);

				XmlSchemaSet schema = new XmlSchemaSet();
				schema.Add("", xmlreader);

				this.Doc.Validate(schema, (sender, e) =>
					{
						result = false;
						//Log.WriteMessage("Erreur validation Xml: " + e.Message, MessageType.Error, MessageLevel.High);  
					});

				return result;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		/// <summary>
		/// Recherche et ajoute le namespace du fichier xml
		/// </summary>
		/// <param name="prefix">prefix à utiliser en reference à ce namespace</param>
		public void AddDefaultNamespace(string prefix)
		{
			var docNamespace = this.Doc.Root.GetDefaultNamespace();
			this.NamespacesManager.AddNamespace(prefix, docNamespace.NamespaceName);
		}

		/// <summary>
		/// Enleve le namespace par défaut
		/// </summary>
		/// <param name="prefix">prefix du namespace</param>
		public void RemoveDefaultNamespace(string prefix)
		{
			var docNamespace = this.Doc.Root.GetDefaultNamespace();

			this.NamespacesManager = new XmlNamespaceManager(new NameTable());
			this.NamespacesManager.RemoveNamespace(prefix, docNamespace.NamespaceName);
		}

		/// <summary>
		/// Lit une valeur à l'adresse xpath specifié
		/// </summary>
		/// <param name="prefixNamespace">prefix namespace à utiliser dans la requète</param>
		/// <param name="xpath">adresse xpath où lire</param>
		/// <returns>Chaine string lue</returns>
		public string ReadSingle(string prefixNamespace, string xpath)
		{
			return ReadSingle(ResolveNamespaceWithXpathQuery(prefixNamespace, xpath));
		}

		/// <summary>
		/// Lit une valeur à l'adresse xpath specifié
		/// </summary>
		/// <param name="xpath">adresse xpath où lire</param>
		/// <returns>Chaine string lue</returns>
		public string ReadSingle(string xpath)
		{
			string result = null;

			try
			{
				var node = this.Doc.XPathSelectElement(xpath, this.NamespacesManager);

				if (node != null)
				{
					if (!String.IsNullOrEmpty(node.Value))
					{
						result = node.Value;
					}
				}
				else
				{
					throw new XPathException("Verifiez chemin XPath, noeud ciblé non trouvé");
				}

				return result;
			}
			catch (XPathException xpathEx)
			{
				XPathException ex = new XPathException("Erreur xpath avec la requete : " + xpath, xpathEx);
				throw ex;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		
		/// <summary>
		/// Lit un tableau de valeurs à l'adresse xpath specifié
		/// </summary>
		/// <param name="prefixNamespace">prefix namespace à utiliser dans la requète</param>
		/// <param name="xpath">adresse xpath où lire</param>
		/// <returns>Tableau de string de valeurs lues</returns>
		public List<string> ReadMultiple(string prefixNamespace, string xpath)
		{
			return ReadMultiple(ResolveNamespaceWithXpathQuery(prefixNamespace, xpath));
		}

		/// <summary>
		/// Lit un tableau de valeurs à l'adresse xpath specifié 
		/// </summary>
		/// <param name="xpath">adresse xpath où lire</param>
		/// <returns>Tableau de string de valeurs lues</returns>
		public List<string> ReadMultiple(string xpath)
		{
			List<string> results = new List<string>();

			try
			{
				var node = this.Doc.XPathSelectElements(xpath, this.NamespacesManager);

				if (node != null)
				{
					foreach (var xel in node)
					{
						if (!String.IsNullOrEmpty(xel.Value))
						{
							results.Add(xel.Value);
						}
					}
				}

				return results;
			}
			catch (XPathException xpathEx)
			{
				XPathException ex = new XPathException("Erreur xpath avec la requete : " + xpath, xpathEx);
				throw ex;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		///// <summary>
		///// Lit un tableau de valeurs à l'adresse xpath specifié 
		///// </summary>
		///// <param name="xpath">adresse xpath où lire</param>
		///// <returns>Tableau de string de valeurs lues</returns>
		//public Dictionary<stri ReadMultiple(string xpath)
		//{
		//    List<XElement> results = new List<XElement>();

		//    try
		//    {
		//        var node = this.Doc.XPathSelectElements(xpath, this.NamespacesManager);

		//        if (node != null)
		//        {
		//            foreach (var xel in node)
		//            {
		//                if (!String.IsNullOrEmpty(xel.Value))
		//                {
		//                    results.Add(xel.Value);
		//                }
		//            }
		//        }

		//        return results;
		//    }
		//    catch (XPathException xpathEx)
		//    {
		//        XPathException ex = new XPathException("Erreur xpath avec la requete : " + xpath, xpathEx);
		//        throw ex;
		//    }
		//    catch (Exception ex)
		//    {
		//        throw ex;
		//    }
		//}

		/// <summary>
		/// Ecrit un element à l'adresse xpath spécifié
		/// </summary>
		/// <param name="prefixNamespace">prefix namespace à utiliser dans la requète</param>
		/// <param name="xpath">adresse xpath où écrire</param>
		/// <param name="elementName">nom de l'element à écrire</param>
		/// <param name="elementValue">valeur(contenu) de l'element</param>
		/// <param name="attributes">liste d'attributs à écrire</param>
		public void WriteElement(string prefixNamespace, string xpath, string elementName, string elementValue, Dictionary<string, string> attributes = null)
		{
			WriteElement(ResolveNamespaceWithXpathQuery(prefixNamespace, xpath), elementName, elementValue, attributes);
		}

		/// <summary>
		/// Ecrit un element à l'adresse xpath spécifié
		/// </summary>
		/// <param name="xpath">adresse xpath où écrire</param>
		/// <param name="elementName">nom de l'element à écrire</param>
		/// <param name="elementValue">valeur(contenu) de l'element</param>
		/// <param name="attributes">liste d'attributs à écrire</param>
		public void WriteElement(string xpath, string elementName, string elementValue, Dictionary<string, string> attributes = null)
		{
			var node = this.Doc.XPathSelectElement(xpath, this.NamespacesManager);

			if (node != null)
			{
				XElement xel = new XElement(elementName, elementValue);

				if (attributes != null && attributes.Count > 0)
				{
					foreach (KeyValuePair<string, string> attribute in attributes)
					{
						xel.Add(new XAttribute(attribute.Key, attribute.Value));
					}
				}

				node.Add(xel);
			}
			else
			{
				throw new XPathException("Verifiez chemin XPath, noeud ciblé non trouvé");
			}
		}

		/// <summary>
		/// Ecrit une valeur à l'adresse xpath spécifié
		/// </summary>
		/// <param name="prefixNamespace">prefix namespace à utiliser dans la requète</param>
		/// <param name="xpath">adresse xpath où écrire</param>
		/// <param name="value">Valeur à écrire</param>
		public void WriteValue(string prefixNamespace, string xpath, string value)
		{
			WriteValue(ResolveNamespaceWithXpathQuery(prefixNamespace, xpath), value);
		}

		/// <summary>
		/// Ecrit une valeur à l'adresse xpath spécifié
		/// </summary>
		/// <param name="xpath">adresse xpath où écrire</param>
		/// <param name="value">Valeur à écrire</param>
		public void WriteValue(string xpath, string value)
		{
			try
			{
				var node = this.Doc.XPathSelectElement(xpath, this.NamespacesManager);

				if (node != null)
				{
					node.Value = value;
				}
				else
				{
					throw new XPathException("Verifiez chemin XPath, noeud ciblé non trouvé");
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Ajoute le namespace correspondant au prefix dans la requète xpath specifiée 
		/// </summary>
		/// <param name="prefix">prefix namespace à utiliser dans la requète</param>
		/// <param name="query">query xpath utilisée</param>
		/// <returns>requète xpath complètée avec le namespace</returns>
		private string ResolveNamespaceWithXpathQuery(string prefix, string query)
		{
			List<string> queryPieces = new List<string>();

			queryPieces.AddRange(query.Split('/'));

			for (int cpt = 0; cpt < queryPieces.Count; cpt++)
			{
				if (!String.IsNullOrEmpty(queryPieces[cpt]))
				{
					queryPieces[cpt] = prefix + ":" + queryPieces[cpt];
				}
			}

			string result = string.Join("/", queryPieces);

			return result;
		}

		/// <summary>
		/// Libère les ressources utilisés par la classe XmlManager
		/// </summary>
		public void Dispose()
		{
			this.Doc = null;
		}
	}
}
