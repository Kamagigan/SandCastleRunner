﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading;

//Namespace regroupant les opérations communes et les classes utilitaires
namespace IC.Common
{
	/// <summary>
	/// Gère le lancement des proccessus pour BuildTasks et des activités custom du Workflow
	/// </summary>
	public class ProcessManager
	{
        /// <summary>
        /// instance de la classe utilitaire de log
        /// </summary>
		LogManager log;
		/// <summary>
		/// Active le tracage du travail des méthodes au niveau de l'objet
		/// </summary>
		public bool ModeDebug { get; set; }
        /// <summary>
        /// attribut d'instance du processus
        /// </summary>
        private Process oProccess { get; set; }

		/// <summary>
		/// Initialise la classe
		/// </summary>
		/// <param name="log">Instance de LogManager à utiliser</param>
		/// <param name="modeDebug">Tracage des actions de la classe</param>
		public ProcessManager(LogManager log =null, bool modeDebug = false)
		{
			this.log = log;

			this.ModeDebug = modeDebug;
		}

		/// <summary>
		/// Démarre un proccessus
		/// </summary>
		/// <param name="processPath">Chemin du proccessus à démarrer</param>
		/// <param name="processArguments">Arguments/paramètres du proccessus à demarrer</param>
		/// <param name="workingDirectory">Dossier de travail pour le proccessus</param>
		/// <param name="processTimeout">temps en milisecondes avant de tuer le proccesus</param>
		/// <param name="useShellExecute">Execute via shell ?</param>
		/// <param name="modeDebug">Active le tracage de la méthode</param>
		/// <returns>Code de retour du proccessus</returns>
		[EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
		public int RunProcess(string processPath, string processArguments, string workingDirectory = null, int processTimeout = 900000, bool useShellExecute = true, bool modeDebug = false)
		{
			try
			{
				//Verification que le script existe à l'endroit indiqué
				if (File.Exists(processPath))
				{
                    //rajout préfix de lancement en fonction de l'extension
                    if (Path.GetExtension(processPath) == ".exe")
                    {
                        processPath.Insert(0, "cmd /c ");

                        if (this.ModeDebug || modeDebug && this.log != null)
                        {
                            this.log.WriteMessage(".exe detecté -> Ajout cmd /c devant", MessageType.Trace);
                        }
                    }
                    else if (Path.GetExtension(processPath) == ".ps1")
                    {
                        processPath.Insert(0, "powershell.exe ");

                        if (this.ModeDebug || modeDebug && this.log != null)
                        {
                            this.log.WriteMessage(".ps1 detecté -> Ajout powershell.exe devant", MessageType.Trace);
                        }
                    }

					ProcessStartInfo oShell = new ProcessStartInfo();
					Process oProcess = null;

					oShell.FileName = processPath;
					oShell.Arguments = processArguments;


					if (!string.IsNullOrEmpty(workingDirectory))
					{
						oShell.WorkingDirectory = workingDirectory;
						
					}

					oShell.WindowStyle = ProcessWindowStyle.Hidden;
					oShell.UseShellExecute = useShellExecute;
					oShell.RedirectStandardOutput = false;

					if (this.ModeDebug || modeDebug && this.log != null)
					{
						//Log des Arguments
						log.WriteMessage(string.Format(Resources.TraceCallRunProcess, processPath), MessageType.Trace, MessageLevel.Default, true);
						log.WriteMessage(string.Format(Resources.msgProcessArgs, processArguments), MessageType.Trace);

                        if (workingDirectory != null)
                        {
                            log.WriteMessage(string.Format(Resources.msgProccessWorkingDirectory, workingDirectory), MessageType.Trace);
                        }
                        else
                        {
                            log.WriteMessage(string.Format("workingDirectory is null"), MessageType.Trace);
                        }

						log.WriteMessage(string.Format(Resources.msgProccessMiscellaneous, processTimeout, useShellExecute.ToString(), oShell.RedirectStandardOutput.ToString()), MessageType.Trace);
					}

					//Demarrage Script
					oProcess = Process.Start(oShell);

					//Parametrage du timeout du Proccesus
					oProcess.WaitForExit(processTimeout);
					if (oProcess.HasExited == false)
					{
						oProcess.Kill();
					}

                    if (this.ModeDebug || modeDebug && this.log != null)
					{
						log.WriteMessage(string.Format(Resources.msgProcessExitCode, oProcess.ExitCode), MessageType.Trace);
					}

					return oProcess.ExitCode;
				}
				else
				{
					log.WriteMessage("Process: " + processPath + " n'existe pas! Abandon lancement process.", MessageType.Error);
					return 0;
				}
			}
			catch (SecurityException SecEx)
			{
				throw SecEx;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        /// <summary>
        /// Démarre un proccessus passé en paramètre avec le flux de sortie redirigé le fichier de log
        /// </summary>
        /// <param name="processPath">Chemin du proccessus à démarrer</param>
        /// <param name="processArguments">Arguments/paramètres du proccessus à demarrer</param>
        /// <param name="workingDirectory">Dossier de travail pour le proccessus</param>
        /// <param name="processTimeout">temps en milisecondes avant de tuer le proccesus (par défaut : 900000 millisencondes)</param>
        /// <param name="ModeDebug">Active le tracage de la méthode</param>
        /// <returns>Code de retour du proccessus</returns>
        [EnvironmentPermissionAttribute(SecurityAction.LinkDemand, Unrestricted = true)]
        public int RunProcessWithOutputRedirectToLog(string processPath, string processArguments, string workingDirectory = null, int processTimeout = 900000, bool ModeDebug = false)
        {
            ProcessStartInfo oProccessInfo = null;
            int exitCode;

            try
            {
                //Verification que le script existe à l'endroit indiqué
                if (File.Exists(processPath) || processPath.Equals("powershell.exe", StringComparison.OrdinalIgnoreCase))
                {
                    oProccessInfo = new ProcessStartInfo();

                    //Création d'un thread de lecture du flux de sortie
                    Thread readInfo = new Thread(new ThreadStart(this.ReadOutput));

                    oProccessInfo.FileName = processPath;
                    oProccessInfo.Arguments = processArguments;

                    if (workingDirectory != null)
                    {
                        oProccessInfo.WorkingDirectory = workingDirectory;
                    }

                    oProccessInfo.CreateNoWindow = true;
                    oProccessInfo.UseShellExecute = false;
                    oProccessInfo.RedirectStandardOutput = true;
                    oProccessInfo.RedirectStandardError = true;

                    if (this.ModeDebug || ModeDebug && this.log != null)
                    {
                        //Log des Arguments
                        log.WriteMessage(string.Format(Resources.TraceCallRunProcess, processPath), MessageType.Trace, MessageLevel.Default, true);
                        log.WriteMessage(string.Format(Resources.msgProcessArgs, processArguments), MessageType.Trace);
                        log.WriteMessage(string.Format(Resources.msgProccessWorkingDirectory, workingDirectory), MessageType.Trace);
                        log.WriteMessage(string.Format(Resources.msgProccessMiscellaneous, processTimeout, oProccessInfo.UseShellExecute.ToString(), oProccessInfo.RedirectStandardOutput.ToString()), MessageType.Trace);
                    }

                    //Demarrage Proccess
                    this.oProccess = Process.Start(oProccessInfo);

                    readInfo.Start();

                    //Parametrage du timeout du Proccesus
                    this.oProccess.WaitForExit(processTimeout);
                    if (this.oProccess.HasExited == false)
                    {
                        this.oProccess.Kill();
                    }

                    if (this.ModeDebug || ModeDebug && this.log != null)
                    {
                        log.WriteMessage(String.Format(Resources.msgProcessExitCode, this.oProccess.ExitCode), MessageType.Trace);
                    }

                    exitCode = this.oProccess.ExitCode;
                    oProccess.Close();
                    return exitCode;
                }
                else
                {
                    if (this.log != null){ 
                        log.WriteMessage("Process: " + oProccessInfo.FileName + " n'existe pas! Abandon lancement process.", MessageType.Error);
                    }

                    throw new Exception("Process: " + oProccessInfo.FileName + " n'existe pas! Abandon lancement process.");
                }
            }
            catch (SecurityException SecEx)
            {
                throw SecEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Méthode de thread pour lire le flux de sortie du processus et l'écrire dans le fichier de log 
        /// </summary>
        private void ReadOutput()
        {
            string currentLine = null;

            Encoding encode = this.oProccess.StandardOutput.CurrentEncoding;

            while (!this.oProccess.HasExited)
            {
                currentLine = this.oProccess.StandardOutput.ReadLine();

                if (!string.IsNullOrEmpty(currentLine))
                {
                    this.log.WriteMessage(ConvertAnsiToUtf8(currentLine), MessageType.Trace);
                }
            }
        }

        /// <summary>
        /// Converti le flux Ansi en UTF8
        /// </summary>
        /// <param name="ansiString">Chaine Ansi</param>
        /// <returns>Chaine UTF8 convertie</returns>
        private string ConvertAnsiToUtf8(string ansiString)
        {
            Encoding ansi = Encoding.GetEncoding(1252);

            Byte[] ansiBytes = ansi.GetBytes(ansiString);

            byte[] utf8Bytes = Encoding.Convert(ansi, Encoding.UTF8, ansiBytes);

            return Encoding.UTF8.GetString(utf8Bytes, 0, utf8Bytes.Length);
        }
	}
}
