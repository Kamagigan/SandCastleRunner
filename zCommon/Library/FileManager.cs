﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;

namespace IC.Common
{
	/// <summary>
	/// Gère les opérations sur les fichiers
	/// </summary>
	public class FileManager : IDisposable
	{
		private LogManager log;
		/// <summary>
		/// Active le tracage du travail des méthodes au niveau de l'objet
		/// </summary>
		public bool ModeDebug { get; set; }

		/// <summary>
		/// Constructeur de Classe 
		/// </summary>
		/// <param name="log">Instance de LogManager</param>
		/// <param name="modeDebug">Active le tracage du travail des méthodes de l'objet</param>
		public FileManager (LogManager log, bool modeDebug = false)
		{
			//mise en attribut du logManager
			this.log = log;

			//mise en attribut du ModeDebug
			this.ModeDebug = modeDebug;
		}

        public static bool IsDirectoryExists (String directoryPath)
        {
            return Directory.Exists(directoryPath);
        }

		/// <summary>
		/// Crée un dossier au chemin indiqué
		/// </summary>
		/// <param name="path">Chemin du dossier</param>
        /// <param name="purgeFolder">purge le dossier existant</param>
		/// <param name="modeDebug">Active le tracage du travail de la méthode</param>
		/// <returns></returns>
		public string CreateFolder(string path, bool purgeFolder = false, bool modeDebug = false)
		{
			DirectoryInfo newFolder;
	   
			try
			{
                if (purgeFolder && Directory.Exists(path))
				{
					DeleteFolder(path);
				}

				newFolder = Directory.CreateDirectory(path);

				if (this.ModeDebug || modeDebug)
					log.WriteMessage(String.Format(Resources.TraceCreateFolder, path), MessageType.Trace);

				return newFolder.FullName;
			}
			catch (Exception ex)
			{
				log.WriteException(ex);
				throw ex;
			}
		}
		
		/// <summary>
		/// Supprime de manière recursive un dossier
		/// </summary>
		/// <param name="path">Emplacement du dossier à supprimer</param>
		/// <param name="modeDebug">Active le tracage du travail de la méthode</param>
		public void DeleteFolder(string path, bool modeDebug = false)
		{
			DirectoryInfo dirInfo;
			List<FileInfo> filesList;
			List<DirectoryInfo> subDirectoriesList;

			if (this.ModeDebug || modeDebug)
                log.WriteMessage("IC.Common FileManager - Suppression dossier: " + path, MessageType.Trace);

			if (Directory.Exists(path))
			{
				//recuperation d'infos
				dirInfo = new DirectoryInfo(path);
				filesList = dirInfo.GetFiles().ToList();
				subDirectoriesList = dirInfo.GetDirectories().ToList();

				try
				{
					//Suppression des fichiers
					if (filesList != null && filesList.Count > 0)
					{
						foreach (FileInfo fileItem in filesList)
						{
							//Gestion des attributs lecture seule
							if (fileItem.IsReadOnly)
								fileItem.IsReadOnly = false;

							fileItem.Delete();
						}
					}

					//Suppression des sous dossiers
					if (subDirectoriesList != null && subDirectoriesList.Count > 0)
					{
						foreach (DirectoryInfo subDirectoryItem in subDirectoriesList)
						{
							DeleteFolder(subDirectoryItem.FullName);
						}
					}

					//Suppression du dossier courant
					dirInfo.Delete();
				}
				catch (Exception ex)
				{
					log.WriteException(ex);
					throw ex;
				}
			}
			else
			{
                DirectoryNotFoundException dirEx = new DirectoryNotFoundException("IC.Common FileManager - Dossier non trouvé: " + path);

				log.WriteException(dirEx);
				throw dirEx;
			}
		}

        /// <summary>
        /// Recherche tous les fichiers avec les extension specifiés en paramètres
        /// </summary>
        /// <param name="path">Chemin du dossier</param>
        /// <param name="extensions">Extensions à chercher</param>
        /// <returns>Liste de FileInfo</returns>
        public List<FileInfo> GetFilesWithExtenions(string path, params string[] extensions)
        {
            DirectoryInfo binFolderInfo = new DirectoryInfo(path);
            List<FileInfo> fileList = binFolderInfo.EnumerateFiles()
                                        .Where(f => extensions.Contains(f.Extension.ToLower()))
                                        .ToList();
            return fileList;
        }

		/// <summary>
		/// Verifie le fichier specifié possede une des extensions en paramètre
		/// </summary>
		/// <param name="fileName">Fichier à tester</param>
		/// <param name="extensions">Extensions valides</param>
		/// <returns>True si le fichier possède une des extensions specifié sinon false</returns>
		public static bool IsFileHaveExtension(string fileName, params string[] extensions)
		{
			try
			{
				FileInfo fileInfo = new FileInfo(fileName);

				foreach (string extension in extensions)
				{
					if (fileInfo.Extension.Contains(extension))
					{
						return true;
					}
				}

				return false;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		// Methodes Private

		/// <summary>
		/// Enleve l'attribut de lecture à une liste de fichiers
		/// </summary>
		/// <param name="filesList">La liste de fichiers</param>
		/// <returns>la liste des fichiers avec l'attribut lecture enlevé</returns>
		private List<FileInfo> RemoveReadOnlyAttribute(List<FileInfo> filesList)
		{
			try
			{
				if (filesList != null && filesList.Count > 0)
				{
					foreach (FileInfo fileItem in filesList)
					{
						if (fileItem.IsReadOnly)
							fileItem.IsReadOnly = false;
					}
				}

				return filesList;
			}
			catch (Exception ex)
			{
				log.WriteException(ex);
				throw ex;
			}
		}

		/// <summary>
		/// Implementation de Idisposable mais rien n'est utilisé en continue
		/// donc rien n'est disposé
		/// </summary>
		public void Dispose()
		{
		}
	}
}
