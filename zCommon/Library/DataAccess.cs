﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.IO;

namespace IC.Common
{
	/// <summary>
	/// Gère les accès à la BDD de BuildTasks et des activités custom du Workflow
	/// </summary>
	public class DataAccess
	{
		string dbConnectionString;

		/// <summary>
		/// Accès à l'objet de log utilisé par la classe
		/// </summary>
		public LogManager log { get; private set; }

		/// <summary>
		/// Tracage de ce qui se passe dans la classe
		/// </summary>
		public bool ModeDebug { get; set; }

		/// <summary>
		/// Retourne le nombre de lignes affectés par la dernière requete sinon -1
		/// </summary>
		public int NbAffectedRows { get; private set; }

		/// <summary>
		/// Constructeur de la Classe DataAccess
		/// </summary>
		/// <param name="log">Instance de LogManager pour logger trace, Erreurs, Exceptions, ...</param>
		/// <param name="connectionString">Surcharge de configuration de base de la connectionString</param>
		/// <param name="ModeDebug">Demarre l'instance de DataAccess en mode Debug (Tracage)</param>
		public DataAccess(string connectionString, LogManager log, bool ModeDebug = false)
		{
			//mise en atribut de connectionString
			this.dbConnectionString = connectionString;

			//mise en attibut du Log
			this.log = log;

			//Ecirture au besoin de connectionString Utilisé
			if (ModeDebug)
				log.WriteMessage(String.Format(Resources.msgConnectionStringUsed, this.dbConnectionString), MessageType.Trace);

			//Intialisation du nombre de lignes affectés
			this.NbAffectedRows = -1;

			//mise en attribut de ModeDebug 
			this.ModeDebug = ModeDebug;
		}

		/// <summary>
		/// Crée un paramètre Sql pour etre uilisé par les aures méthodes de la classe
		/// </summary>
		/// <param name="name">Nom du paramètre</param>
		/// <param name="value">Valeur du paramètre</param>
		/// <param name="type">Type du paramètre</param>
		/// <returns>Paramètre Sql crée</returns>
		public SqlParameter CreateSqlParameter(string name, object value, SqlDbType type)
		{
			try
			{
				SqlParameter parameter = new SqlParameter(name, type);

				if (value == null)
				{
					parameter.Value = DBNull.Value;
				}
				else
				{
					if (value.GetType() == typeof(DateTime))
					{
						value = DateTimeHelper.FormatDateTimeToAmericanFormat((DateTime)value);
					}

					parameter.Value = value;
				}

				return parameter;
			}
			catch (Exception ex)
			{
				log.WriteException(ex);
				throw ex;
			}
		}

		/// <summary>
		/// Execute une proccédure stockée sur la BDD
		/// </summary>
		/// <param name="procedureName">Nom de la proccédure stocké à appeler</param>
		/// <param name="parameters">Liste des parametres à fournir</param>
		/// <param name="returnType">Type de retour attentu</param>
		/// <param name="modeDebug">Active le tracage du travail de la méthode</param>
		/// <returns>DataSet contenant le resultat de la procédure stockée</returns>
		public DataSet ExecStoredProcedure(string procedureName, List<SqlParameter> parameters, SqlDbType returnType = SqlDbType.Int, bool modeDebug = false)
		{
			 DataSet res = ExecStoredProcedure(procedureName, parameters, 0, returnType, modeDebug);

			return res;
		}

		/// <summary>
		/// Execute une proccédure stockée sur la BDD
		/// </summary>
		/// <param name="procedureName">Nom de la proccédure stocké à appeler</param>
		/// <param name="parameters">Liste des parametres à fournir</param>
		/// <param name="timeOut">TimeOut de la requete</param>
		/// <param name="returnType">Type de retour attentu</param>
		/// <param name="modeDebug">Active le tracage du travail de la méthode</param>
		/// <returns>DataSet contenant le resultat de la procédure stockée</returns>
		public DataSet ExecStoredProcedure(string procedureName, List<SqlParameter> parameters, int timeOut, SqlDbType returnType = SqlDbType.Int, bool modeDebug = false)
		{
			DataSet result = null;
			SqlCommand query = null;
			SqlDataAdapter sqlAdapter = null;

			try
			{
				if (this.ModeDebug || modeDebug)
				{ 
					//Log de l'appel
					log.WriteMessage(String.Format(Resources.TraceCallStoredProcedure, procedureName, returnType.ToString()), MessageType.Trace, MessageLevel.Default, true);
				}

				using (SqlConnection connection = new SqlConnection(this.dbConnectionString))
				{
					//Initialisation DataSet de resultat
					result = new DataSet();
					result.Locale = CultureInfo.InvariantCulture;
					result.DataSetName = procedureName;
					result.EnforceConstraints = false;

					//Parametrage de la requete
					query = new SqlCommand();
					query.Connection = connection;
					query.CommandText = procedureName;
					query.CommandType = CommandType.StoredProcedure;
					if (timeOut > 0) { query.CommandTimeout = timeOut; }

					//Ouverture connexion
					if (connection.State == ConnectionState.Closed || connection.State == ConnectionState.Broken)
					{
						connection.Open();
					}

					if (this.ModeDebug || modeDebug)
					{
						//Log du nombe de parametres utilisé
						log.WriteMessage(String.Format(Resources.msgNumberOfParametersUsed, parameters.Count.ToString()), MessageType.Trace);
					}

					if (parameters != null && parameters.Count > 0)
					{
						//Ajout parametres à la requete
						foreach (SqlParameter parameter in parameters)
						{
							if (this.ModeDebug || modeDebug)
							{
								//Log des paramètres utilisés
								log.WriteMessage(String.Format(Resources.msgParameterUsed, parameter.ParameterName, parameter.SqlDbType.ToString(), parameter.Value.ToString()), MessageType.Trace);
							}

							query.Parameters.Add(parameter);
						}
					}
	
					//execution requete
					sqlAdapter = new SqlDataAdapter();
					sqlAdapter.SelectCommand = query;
					this.NbAffectedRows = sqlAdapter.Fill(result);

					if (this.ModeDebug || modeDebug)
					{
						log.WriteMessage(String.Format(Resources.msgNumberofLinesAffected, this.NbAffectedRows.ToString()), MessageType.Trace);
					}
				}
			}
			catch (Exception ex)
			{
				log.WriteException(ex);
				throw ex;
			}

			return result;
		}

		/// <summary>
		/// Permet de lancer un script sql, récupération du texte du fichier et execution SqlCommand
		/// </summary>
		/// <param name="scriptFile">Fichier contenant le script à exécuter</param>
		/// <param name="modeDebug">Active le tracage de la methode</param>
		public void ExecSqlScript(string scriptFile, bool modeDebug = false)
		{
			FileStream fileStream = null;
			StreamReader streamReader = null;
			SqlCommand command = null;
			SqlConnection connection = null;

			StringBuilder sb = null;
			string currentLine = null;

			try
			{
				//Ouverture Fichier
				fileStream = new FileStream(scriptFile, FileMode.Open, FileAccess.Read);
				streamReader = new StreamReader(fileStream);

				//Peparation requete
				connection = new SqlConnection(this.dbConnectionString);
				command = connection.CreateCommand();
				command.CommandType = CommandType.Text;

				//ouverture connexion
				connection.Open();

				//Boucle 
				while (!streamReader.EndOfStream)
				{
					sb = new StringBuilder();

					//Boucle jusqu'au prochain GO
					while (!streamReader.EndOfStream)	
					{
						currentLine = streamReader.ReadLine();

						if (!String.IsNullOrEmpty(currentLine) && currentLine.Trim().Equals("GO", StringComparison.OrdinalIgnoreCase))
						{
							break;
						}

						sb.AppendLine(currentLine);
					}

					command.CommandText = sb.ToString();

					if (this.ModeDebug || modeDebug)
					{
						this.log.WriteMessage(command.CommandText, MessageType.Trace);
					}

					command.ExecuteNonQuery();
				}
			}
			catch (Exception ex)
			{
				this.log.WriteMessage("Exception dans " + scriptFile + " : " + ex.Message + Environment.NewLine + "StackTrace : " + ex.StackTrace, MessageType.Error);

				throw new Exception(scriptFile + " " + ex.Message, ex);
			}
			finally
			{
				if ((connection != null) && (connection.State == ConnectionState.Open)){ connection.Dispose(); }
				if (command != null){ command.Dispose(); }
				if (streamReader != null){ streamReader.Dispose(); }
				if (fileStream != null){ fileStream.Dispose(); }
			}
		}
	}
}
