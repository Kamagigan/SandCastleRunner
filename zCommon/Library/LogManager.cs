﻿using System;
using System.IO;

//Namespace regroupant les opérations communes et les classes utilitaires
namespace IC.Common
{
	/// <summary>
	/// Gère l'ecriture du log en texte brut 
	/// </summary>
	public class LogManager : IDisposable
	{
		/// <summary>
		/// Emplacement du fichier où font écrit les logs
		/// </summary>
		public string logFilePath { get; private set; }
        /// <summary>
        /// Stream d'écriture dans le fichier
        /// </summary>
		StreamWriter sw = null;
        /// <summary>
        /// Garde le StreamWriter ouvert ?
        /// </summary>
		bool keepAlive = false;

		/// <summary>
		/// Constructeur de classe
		/// </summary>
		/// <param name="LogFilePath">Nom de fichier où écrire</param>
		/// <param name="keepAlive">Fermetture automatique(false) ou non(true) du flux d'ecriture ?</param>
		public LogManager(string LogFilePath, bool keepAlive = false)
		{
			if (!String.IsNullOrEmpty(LogFilePath))
			{
				this.logFilePath = LogFilePath;
				this.keepAlive = keepAlive;

                if (!File.Exists(logFilePath))
                {
                    string FileDirectory = Path.GetDirectoryName(logFilePath);

                    if (!String.IsNullOrEmpty(FileDirectory) && !Directory.Exists(FileDirectory))
                    {
                        Directory.CreateDirectory(FileDirectory);
                    }

                    FileStream fs = File.Open(logFilePath, FileMode.CreateNew, FileAccess.ReadWrite);
                    fs.Dispose();
                }

				if (keepAlive)
				{
					this.sw = GetWriter();
				}
			}
			else
			{
				throw new Exception(String.Format(Resources.msgPathIsNull, "Log"));
			}
		}

		/// <summary>
		/// Recupère ou instancie le flux d'ecriture dans un fichier 
		/// </summary>
		/// <returns>Flux d'ecriture</returns>
		private StreamWriter GetWriter()
		{
			if (this.sw != null)
			{
				return this.sw;
			}
			else
			{
				return new StreamWriter(logFilePath, true, System.Text.Encoding.UTF8);
			}
		}

		/// <summary>
		/// Ferme le flux d'ecriture passé en paramètre
		/// </summary>
		/// <param name="sw">Le flux d'ecriture à fermé</param>
		private void DisposeWriter(StreamWriter sw)
		{
			if (sw != null)
			{
				sw.Close();
				sw.Dispose();
			}
		}

		/// <summary>
		/// Ecrit un message dans le log
		/// </summary>
		/// <param name="textMessage">Message à écrire</param>
		/// <param name="type">Type de message</param>
		/// <param name="level">Importance du message</param>
		/// <param name="AddBlankLineBefore">Ajout d'une ligne vide avant l'ecriture du message ?</param>
		/// <param name="AddBlankLineAfter">Ajout d'une ligne vide après l'ecriture du message ?</param>
		public void WriteMessage(string textMessage, MessageType type = MessageType.Default, MessageLevel level = MessageLevel.Default, Boolean AddBlankLineBefore = false, Boolean AddBlankLineAfter = false)
		{
			try
			{
				StreamWriter sw = GetWriter();

				if (AddBlankLineBefore==true)
					sw.WriteLine(" ");
				sw.WriteLine(DateTime.Now.ToString() + " : " + type + " / " + level + " : " + textMessage);
				if (AddBlankLineAfter == true)
					sw.WriteLine(" ");

				if (!keepAlive)
					this.DisposeWriter(sw);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Ecrit une exception dans le log 
		/// </summary>
		/// <param name="exception"></param>
		public void WriteException(Exception exception)
		{
			try
			{
                Exception currentException = exception;

				if (!String.IsNullOrEmpty(exception.Message))
				{
					StreamWriter sw = GetWriter();

					if (!String.IsNullOrEmpty(exception.Source)){
						sw.WriteLine(DateTime.Now.ToString() + " : " + MessageType.Exception + " / Message : " + exception.Message);
					}

					if (!String.IsNullOrEmpty(exception.StackTrace)){
						sw.WriteLine("StackTrace: " + exception.StackTrace);
					}

                    while (currentException.InnerException != null)
                    {
                        currentException = currentException.InnerException;

                        sw.WriteLine("Inner Exception Message : " + currentException.Message);
                        sw.WriteLine("StackTrace : " + currentException.StackTrace);
                    }

					if (!keepAlive){
						this.DisposeWriter(sw);
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Ferme et libère toutes les ressources utilisés par le LogManager
		/// </summary>
		public void Dispose()
		{
			if (this.sw != null)
			{
				this.DisposeWriter(this.sw);
			}
		}
	}
}
