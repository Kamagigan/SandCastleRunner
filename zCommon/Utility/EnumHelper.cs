﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IC.Common
{
	/// <summary>
	///  Contient des methodes utilitaires concernant l'objet Enum
	/// </summary>
	public class EnumHelper
	{
		/// <summary>
		/// Parse une chaine string en Enum de type T
		/// </summary>
		/// <param name="value">Value string à parser en enum</param>
		/// <returns>Enum de type T parsé</returns>
		public static T ParseEnum<T>(string value)
		{
			return (T)Enum.Parse(typeof(T), value, true);
		}

		public static bool IsValid<T>(string value)
		{
			return Enum.IsDefined(typeof(T), value);
		}
	}
}
