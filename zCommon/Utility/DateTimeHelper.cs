﻿using System;

namespace IC.Common
{
	/// <summary>
	///  Contient des methodes utilitaires concernant l'objet DateTime
	/// </summary>
	public class DateTimeHelper
	{
		/// <summary>
		/// Valide un objet Datetime si il n'est null ou initialisé à 01/01/0001
		/// </summary>
		/// <param name="dateTime">l'objet DateTime à tester</param>
		/// <returns>true si valide, false sinon</returns>
		public static bool isDateTimeValid(DateTime dateTime)
		{
			DateTime dateTimeNull = new DateTime();

			if (dateTime != null && dateTime != dateTimeNull)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// Formate un objet Datetime au format américain année-mois-jour heure:minutes:secondes 
		/// </summary>
		/// <param name="datetime">l'objet DateTime à formater</param>
		/// <returns>la chaine string formaté</returns>
		public static string FormatDateTimeToAmericanFormat(DateTime datetime)
		{
			return String.Format("{0:yyyy-MM-d HH:mm:ss}", datetime);
		}
	}
}
