﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using IC.Common;

//Namespace regroupant les opérations de génération de documentation à l'aide de l'outil SandCastle Help File Builder
namespace IC.SandCastleGeneration
{
    /// <summary>
    /// Represente un fichier de projet SandCastle
    /// </summary>
    public class SandCastleProject
    {
        /// <summary>
        /// Chemin vers le fichier template .shbf utilisé comme structure de projet
        /// </summary>
        public string TemplatePath { get; set; }
        /// <summary>
        /// Chemin où est enregistré le projet SandCastle
        /// </summary>
        public string ProjectPath { get; private set; }

        /// <summary>
        /// Nom de la documentation
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Titre de la documentation
        /// </summary>
        public string HelpTitle { get; set; }

        //par défaut en français
        private string _Language = "fr-FR";
        /// <summary>
        /// Langue de Documentation
        /// </summary>
        public string Language
        {
            get
            {
                return this._Language;
            }

            set
            {
                this._Language = value;
            }
        }

        /// <summary>
        /// Version du framework .Net utilisé 
        /// </summary>
        public string FrameworkVersion { get; set; }

        //par défaut en site web
        private string _OutputFormat = "WebSite";
        /// <summary>
        /// Format de sortie de documentations
        /// </summary>
        public string OutputFormat 
        {
            get
            {
                return this._OutputFormat;
            }

            set
            {
                this._OutputFormat = value;
            }
        }

        /// <summary>
        /// Chemin de travail de sandCastle
        /// </summary>
        public string WorkingPath { get; set; }
        /// <summary>
        /// Chemin de sortie de la  documentation 
        /// </summary>
        public string OutputPath { get; set; }
        
        /// <summary>
        /// Liste des sources de documentation (.exe/dll et .xml)
        /// </summary>
        public List<string> DocumentationSources { get; set; }
        /// <summary>
        /// Liste des references néccésaire à la documentation
        /// </summary>
        public List<string> ReferencesSources { get; set; }

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public SandCastleProject()
        {

        }

        /// <summary>
        /// Copie le template choisi au chemin indiqué en paramètre et met à jour les informations décidés pour le projet SandCastle
        /// </summary>
        /// <param name="path">Chemin où doit etre enregistré le projet SandCastle</param>
        public void Save (string path)
        {
            //mise en attribut du chemin du projet
            this.ProjectPath = path;

            //Copie du template
            try
            {
                File.Copy(this.TemplatePath, this.ProjectPath, true);
            }
            catch (Exception ex)
            {
                throw new Exception("Impossible de copier fichier fichier template de " + this.TemplatePath + " à " + this.ProjectPath, ex);
            }

            //Ouverture, xml du fichier projet  
            XmlManager oXml = XmlManager.getInstance();

            oXml.LoadDocument(this.ProjectPath, true);

            //recuperation du namespace xml du fichier
            string prefixNamespace = "ns";
            oXml.AddDefaultNamespace(prefixNamespace);

            //Mise à jour des informations du projet
            try
            {
                oXml.WriteValue(prefixNamespace, "//PropertyGroup[1]/HelpTitle", this.HelpTitle);
                oXml.WriteValue(prefixNamespace, "//PropertyGroup[1]/HtmlHelpName", this.Name);
                oXml.WriteValue(prefixNamespace, "//PropertyGroup[1]/FrameworkVersion", this.FrameworkVersion);
                oXml.WriteValue(prefixNamespace, "//PropertyGroup[1]/Language", this.Language);
                oXml.WriteValue(prefixNamespace, "//PropertyGroup[1]/HelpFileFormat", this.OutputFormat);
                oXml.WriteValue(prefixNamespace, "//PropertyGroup[1]/OutputPath", this.OutputPath);
                oXml.WriteValue(prefixNamespace, "//PropertyGroup[1]/WorkingPath", this.WorkingPath);

                foreach (string source in this.DocumentationSources)
                {
                    Dictionary<string, string> attributs = new Dictionary<string, string>();
                    attributs.Add("sourceFile", source);
                    attributs.Add("xmlns", "");

                    oXml.WriteElement(prefixNamespace, "//PropertyGroup[1]/DocumentationSources", "DocumentationSource", null, attributs);
                }

                if (this.ReferencesSources != null && this.ReferencesSources.Count > 0)
                {
                    foreach (string source in this.DocumentationSources)
                    {
                        Dictionary<string, string> attributs = new Dictionary<string, string>();
                        attributs.Add("sourceFile", source);
                        attributs.Add("xmlns", "");

                        oXml.WriteElement(prefixNamespace, "//PropertyGroup[1]/DocumentationSources", "DocumentationSource", null, attributs);
                    }
                }

                //sauvegarde effective des modifications  
                oXml.Save();
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur Sauvegarde Fichier project SandCastle", ex);
            }
        }
    }
}
