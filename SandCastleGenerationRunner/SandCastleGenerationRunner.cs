﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using IC.Common;
using System.Configuration;

//Namespace regroupant les opérations de l'executable SandCastleGeneration
namespace IC.SandCastleGeneration
{
    /// <summary>
    /// Classe interface console lancant le proccessus de génération de la documentation
    /// </summary>
    class SandCastleGenerationRunner
    {
        /// <summary>
        /// Méthode appellé au lancment du programme
        /// </summary>
        /// <param name="args">arguments passés au programme</param>
        static int Main(string[] args)
        {
            //nombre d'arguments utilisés
            int paramNumber = 3;
            //Chemin vers les fichiers binaires du projet
            string binFolder = null;
            //Chemin de sortie pour la documentation généré
            string outputFolder = null;
            //Chemin vers le fichier template .shbf utilisé comme structure de projet
            string TemplateSandCastle = null;
            //Chemin vers le fichier .xml de la configuration propre au projet
            string configFile = null;

            //instance de LogManager
            LogManager oLog;

            //si le nombre d'arguments fournis est differents au nombre attendue, affichage de l'aide
            if (args.Length != paramNumber)
            {
                Console.WriteLine("SandCastleGeneration - Programme de génération de la documentation");
                Console.WriteLine("ERREUR : " + paramNumber + " paramètres requis (" + args.Length.ToString() + " actuellement)");
                Console.WriteLine(" Syntaxe d'appel : SandCastleGenerationRunner.exe [binFolder] [outputFolder] [configFile]");

                return -1;
            }
          else
            {
                //Données de test
                binFolder = @"G:\_lcudini\TFS\TEST_SandCastleGeneration\IntegrationContinue_Jour_TemplateIC\bin";
                outputFolder = @"G:\_lcudini\TFS\TEST_SandCastleGeneration\IntegrationContinue_Jour_TemplateIC\bin\help";
                configFile = @"G:\_lcudini\TFS\TEST_SandCastleGeneration\IntegrationContinue_Jour_TemplateIC\src\IntégrationContinueConfig.xml";


                try
                {
                    //Lecture des arguments d'entrée
                    binFolder = args[0];
                    outputFolder = args[1];
                    configFile = args[2];
                }
                catch (Exception ex)
                {
                    Console.WriteLine("SandCastleGeneration - Programme de génération de la documentation");
                    Console.WriteLine("Erreur de lecture des paramètres d'entrée du programme");
                    Console.WriteLine("Detail de l'exception : " + ex.Message);
                    Console.WriteLine("StackTrace : " + ex.StackTrace);

                    return -1;
                }

                try
                {
                    //demarrage log 
                    oLog = new LogManager(binFolder + @"\Logs\SandCastleGenerationRunner.log");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Erreur creation fichier de log", ex);

                    return -1;
                }

                //recuperation du temps actuel
                DateTime now = DateTime.Now;

                //log paramètres d'entrée
                oLog.WriteMessage("///////////////////////// SandCastleGeneration Log - " + now.Day + "/" + now.Month + "/" + now.Year + " " + now.Hour + ":" + now.Minute + ":" + now.Second + " ///////////////////////");
                oLog.WriteMessage("BinFolder : " + binFolder, MessageType.Trace);
                oLog.WriteMessage("OutputFolder : " + outputFolder, MessageType.Trace);
                oLog.WriteMessage("Project configFile : " + configFile, MessageType.Trace);

                //recuperation et log des paramètres du fichier de config global
                string msbuildPath = GetMSBuildPath();
                oLog.WriteMessage("Chemin MSBuild : " + msbuildPath, MessageType.Trace);

                TemplateSandCastle = GetSandCastleTemplatePath();
                oLog.WriteMessage("Template utilisé : " + TemplateSandCastle, MessageType.Trace);

                List<string> communBinariesExclusionList = GetCommunBinariesExclusionList();
                oLog.WriteMessage("Exclusion Binaries communes : ", MessageType.Trace);

                foreach (string exclusion in communBinariesExclusionList)
                {
                    oLog.WriteMessage(exclusion, MessageType.Trace);
                }

                try
                {
                    //Création de la classe SandcastleGeneration pour générer la doc 
                    SandCastleGeneration sandCastle = new SandCastleGeneration(msbuildPath, TemplateSandCastle, communBinariesExclusionList);

                    //lancement de la génération 
                    sandCastle.GenerateDocumentation(binFolder, outputFolder, configFile, oLog);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw ex;
                }
            }

            return 0;
        }

        /// <summary>
        /// Récuperation du chemin vers MSBuild dans le fichier de config de l'application
        /// </summary>
        /// <returns>Chemin vers MSBuild à utiliser</returns>
        private static string GetMSBuildPath()
        {
            return ConfigurationManager.AppSettings["MSBuildPath"];
        }

        /// <summary>
        /// Récuperation de la liste des dll à exclure dans toutes les génération de doc
        /// </summary>
        /// <returns>Liste des dll à exclure d'office</returns>
        private static List<string> GetCommunBinariesExclusionList ()
        {
            string strCommunBinariesExclusion = ConfigurationManager.AppSettings["CommunBinariesExclusionList"];

            return strCommunBinariesExclusion.Split(';').ToList();
        }

        /// <summary>
        /// Récuperation du chemin vers le template de projet sandcastle à utilisé
        /// </summary>
        /// <returns>Chemin vers le fichier de template de projet</returns>
        private static string GetSandCastleTemplatePath()
        {
            return ConfigurationManager.AppSettings["SandCastleTemplate"];
        }
    }
}
