﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using IC.Common;

//Namespace regroupant les operations de generation de documentation à l'aide de l'outil SandCastle Help File Builder
namespace IC.SandCastleGeneration
{
    /// <summary>
    /// Classe gérant la génération du fichier projet SandCastle et sa compilation pour générer la documentation du projet
    /// </summary>
    public class SandCastleGeneration
    {
        /// <summary>
        /// Chemin vers les fichiers binaires du projet
        /// </summary>
        public string BinFolder { get; set; }
        /// <summary>
        /// Chemin de sortie pour la documentation généré 
        /// </summary>
        public string OutputFolder { get; set; }
        /// <summary>
        /// Chemin vers le dossier où SandCastle travaillera
        /// </summary>
        public string SandCastleWorkingFolder { get; set; }

        /// <summary>
        /// Chemin vers l'executable MSBuild néccésaire à la génération de la documentation
        /// </summary>
        public string MsBuildPath { get; set; }
        /// <summary>
        /// Chemin vers le fichier template .shbf utilisé comme structure de projet
        /// </summary>
        public string SandCastleGenerationTemplatePath { get; set; }
        /// <summary>
        /// Liste des fichiers exclus de la documentation pour tous les projets + ceux spécifiés dans le fichier de configuration du projet (Regex acceptés)
        /// </summary>
        public List<string> BinariesExclusionList { get; set; }
        /// <summary>
        /// Durée de timeout du proccessus de génération de documentation
        /// </summary>
        public int DocGenerationTimeOut { get; set; }
		
        /// <summary>
        /// Instance de classe gérant le log des actions de la classe
        /// </summary>
        public LogManager oLog { get; private set; }
        /// <summary>
        /// Instance de classe asistant les opérations de fichier de la classe
        /// </summary>
        private FileManager oFileManager = null;

        /// <summary>
        /// Constructeur de la classe
        /// </summary>
        /// <param name="msbuildPath">Chemin vers l'executable MSBuild néccésaire à la génération de la documentation</param>
        /// <param name="sandCastleTemplatePath">Chemin vers le fichier template .shbf utilisé comme structure de projet</param>
        /// <param name="communBinariesExclusionList">Liste des fichiers exclus de la documentation pour tous les projets (Regex acceptés)</param>
        public SandCastleGeneration(string msbuildPath, string sandCastleTemplatePath, List<string> communBinariesExclusionList = null)
        {
            this.MsBuildPath = msbuildPath;
            this.SandCastleGenerationTemplatePath = sandCastleTemplatePath;
            this.BinariesExclusionList = communBinariesExclusionList;
        }

        /// <summary>
        /// Lance la génération du projet SandCastle et de la documentation associé
        /// </summary>
        /// <param name="binFolder">Chemin vers les fichiers binaires du projet</param>
        /// <param name="outputFolder">Chemin de sortie pour la documentation généré </param>
        /// <param name="projectConfigFile">Chemin vers le fichier .xml de la configuration propre au projet</param>
        /// <param name="logManager">Instance de LogManager pour enregistrer les logs de la génération</param>
        /// <returns>Booléen indiquant si la génération de la documentation à reussie ou non</returns>
        public bool GenerateDocumentation(string binFolder, string outputFolder, string projectConfigFile, LogManager logManager)
        {
            //mise en attribut de ses paramètres
            this.BinFolder = binFolder;
            this.OutputFolder = outputFolder;
            this.oLog = logManager;

            //boolean de reussite final
            bool isSuccess = false;

            //compteur d'erreur de conf
            int confError = 0;

            //Projet SandCastle
            SandCastleProject docProject = new SandCastleProject();

            //Generate Doc appellé
            oLog.WriteMessage("GenerateDocumentation appellé", MessageType.Trace);

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Vérifications
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (!File.Exists(this.MsBuildPath)){
                oLog.WriteMessage("Chemin vers MSBuild inexistant ou chemin incorrect. Chemin Actuel : " + this.MsBuildPath, MessageType.Error);
                confError++;
            }

            if (!File.Exists(this.SandCastleGenerationTemplatePath)){
                oLog.WriteMessage("Template SandCastle inexistant ou chemin incorrect. Chemin Actuel : " + this.SandCastleGenerationTemplatePath, MessageType.Error);
                confError++;
            }

            if (!Directory.Exists(this.BinFolder))
            {
                oLog.WriteMessage("Dossier Binaries inexistant ou chemin incorrect. Chemin Actuel : " + this.BinFolder, MessageType.Error);
                confError++;
            }

            if (!File.Exists(projectConfigFile))
            {
                oLog.WriteMessage("Fichier de configuration projet inexistant ou chemin incorrect. Chemin Actuel : " + projectConfigFile, MessageType.Error);
                confError++;
            }

            if (confError > 0)
            {
                throw new Exception(confError + " Erreur(s) de configuration détecté, consultez le fichier de log pour plus de détail : " + this.oLog.logFilePath);
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Creation d'un espace de travail pour sandCastle
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            oFileManager = new FileManager(this.oLog);

            this.SandCastleWorkingFolder = binFolder + @"\TmpSandCastle";
            oLog.WriteMessage("Dossier temp SandCastle prévu : " + this.SandCastleWorkingFolder, MessageType.Trace);

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Récuperation informations fichier de configuration
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            oLog.WriteMessage("Lecture fichier de configuration projet : " + projectConfigFile, MessageType.Trace);

            XmlManager oXml = XmlManager.getInstance();
            oXml.LoadDocument(projectConfigFile, true);

            //Noms et titres de la documentation
            docProject.Name = oXml.ReadSingle("//SandCastle/projectName");
            docProject.HelpTitle = oXml.ReadSingle("//SandCastle/helpTitle");

            //Version du frameworjk .Net du projet
            docProject.FrameworkVersion = oXml.ReadSingle("//SandCastle/frameworkVersion");

            //format de sortie de la documentation (.chm / Website)
            docProject.OutputFormat = oXml.ReadSingle("//SandCastle/outputFormat");

            //langue de la documentation
            docProject.Language = oXml.ReadSingle("//SandCastle/language");

            //Réferences de projet (si besoin, pas été testé)
            docProject.ReferencesSources = oXml.ReadMultiple("//SandCastle/projectReferences");

            //Chemin du template à utiliser pour la création de la documentation
            docProject.TemplatePath = SandCastleGenerationTemplatePath;
            
            //Chemein du dossier de sortie de la documentation
            docProject.OutputPath = this.OutputFolder;

            //Chemin du dossier de travauil de SandCastle
            docProject.WorkingPath = this.SandCastleWorkingFolder;

            //Lecture de la liste des fichiers binairs à exclure pour ce projet et ajout à la liste d'exclusion commune
            List<string> customBinariesExclusion = oXml.ReadMultiple("//SandCastle/binariesExclusion");
            if (customBinariesExclusion != null && customBinariesExclusion.Count > 0)
            {
                this.BinariesExclusionList.AddRange(customBinariesExclusion);
            }

            //Liberation de la classe xml
            oXml.Dispose();

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // RECHERCHE FICHIERS A DOCUMENTER
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            docProject.DocumentationSources = GetDocumentationSourceList(this.BinFolder, this.BinariesExclusionList);

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // FIN CONSTRUCTION ET SAUVEGARDE PROJET SANDCASTLE
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            try
            {
                docProject.Save(binFolder + @"\Logs\" + docProject.Name + ".shfbproj");

                oLog.WriteMessage("Projet SandCastle enregistré à l'adresse : " + docProject.ProjectPath, MessageType.Trace);
            }
            catch (Exception ex)
            {
                oLog.WriteException(ex);
                return false;
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // GENERATION DOC / RUN MSBUILD
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            oLog.WriteMessage("Démmarage proccesus MSBuild sur le projet SandCastle enregistré", MessageType.Trace);

            try
            {
                isSuccess = BuildSandCastleProject(docProject.ProjectPath);
                
                if (isSuccess)
                {
                    oLog.WriteMessage("Generation documentation terminé et sans erreur, veuillez verifier le resultat au chemin suivant : " + this.OutputFolder);
                }
                else
                {
                    oLog.WriteMessage("Generation documentation terminé et avec erreur(s), veuillez consulter le log au chemin suivant : " + this.oLog.logFilePath);
                }       
            }
            catch (Exception ex)
            {
                oLog.WriteException(ex);
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // NETTOYAGE
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Retour
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            return isSuccess;
        }

        /// <summary>
        /// Compile et génére la documentation associé au fichier de projet SandCastle passé en paramètres
        /// </summary>
        /// <param name="projectFile">Fichier de projet SandCastle</param>
        /// <returns></returns>
        public bool BuildSandCastleProject(string projectFile)
        {
            //Démarrage de la classe utilitaire de gestion des proccessus en passant l'instance de logManager
            ProcessManager oProcessManager = new ProcessManager(this.oLog);
            bool isSuccess = false;

            try
            {
                //demarrage du processus de compilation du fichier de projet SandCastle en redirigant la sortie vers le fichier de log 
                int exitCode = oProcessManager.RunProcessWithOutputRedirectToLog(this.MsBuildPath, "\"" +  projectFile + "\"");

                //si reussite du proccessus (exitCode 0), la génération est un succès
                if (exitCode == 0)
                {
                    isSuccess = true;
                }
                else
                {
                    isSuccess = false;
                }

            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }

            return isSuccess;
        }

        /// <summary>
        /// Recupère la liste des .exe et .dll à documenter avec leurs fichiers .xml associés parmi le dossier des binaires du projet
        /// </summary>
        /// <param name="binFolder">Chemin vers les fichiers binaires du projet</param>
        /// <param name="exclusionList">Liste des fichiers exclus de la documentation pour tous les projets (Regex acceptés)</param>
        /// <returns>Liste des fichiers à documenter</returns>
        private List<string> GetDocumentationSourceList(string binFolder, List<string> exclusionList)
        {
            List<string> DocumentationSourceList = new List<string>();
            List<FileInfo> dllfileList = new List<FileInfo>();
            List<Regex> exclusionRules = new List<Regex>();

            this.oLog.WriteMessage("Recherche fichiers à documenter", MessageType.Trace);

            //récuperation des fichiers avec l'extension .exe ou .dll
            dllfileList = this.oFileManager.GetFilesWithExtenions(this.BinFolder, ".exe", ".dll");
            oLog.WriteMessage("Trouvé " + dllfileList.Count + " fichiers Dll/Exe", MessageType.Trace);

            //Cast de l'exclusionList List<string> en List<Regex>
            if (exclusionList != null && exclusionList.Count > 0)
            {
                foreach (string exclusion in exclusionList)
                {
                    exclusionRules.Add(new Regex(exclusion, RegexOptions.IgnoreCase));
                }
            }

            //Pour chaque fichier trouvé
            foreach (FileInfo dllfile in dllfileList)
            {
                //Vérification si le fichier n'est pas dans la liste des exclusions binaires
                if (isFileToBeAnalysed(dllfile.Name, exclusionRules))
                {
                    //recherche de l'equivalent dui fichier au format xml
                    string xmlFile = dllfile.DirectoryName + @"\" + Path.GetFileNameWithoutExtension(dllfile.Name) + ".xml";

                    if (File.Exists(xmlFile))
                    {
                        //si l'equivalent xml existe alors ajout des deux fichiers dans la liste des sources de documentation
                        oLog.WriteMessage("Fichier xml de documentation trouvé pour " + dllfile.Name);
                        DocumentationSourceList.Add(dllfile.FullName);
                        DocumentationSourceList.Add(xmlFile);
                    }
                    else
                    {
                        //Sinon on informe de l'impossibilité de trouver le fichier xml equivalent
                        oLog.WriteMessage("Fichier xml de documentation manquant pour " + dllfile.Name, MessageType.Trace);
                    }
                }
            }

            //information du nombre final de source de documentation
            oLog.WriteMessage("Compte final sources de documenation : " + DocumentationSourceList.Count / 2 + " avec leurs fichiers xml", MessageType.Trace);
            return DocumentationSourceList;
        }

        /// <summary>
        /// Vérifie si le fichier passé en paramètre ne correspond pas aux regex de la liste des exclusions binaires
        /// </summary>
        /// <param name="filename">nom de fichier à tester</param>
        /// <param name="exclusionRules">Liste des regex définissant la liste des exclusion binaires</param>
        /// <returns>Booléen indiquant si le fichier doit être traiter par la suite (true) ou non (false)</returns>
        private bool isFileToBeAnalysed(string filename, List<Regex> exclusionRules)
        {
            if (exclusionRules != null && exclusionRules.Count > 0)
            {
                foreach (Regex exclusionRule in exclusionRules)
                {
                    if (exclusionRule.IsMatch(filename))
                    {
                        //Si une règle d'exclusion binaire correspond avec un nom de fichier, le fichier n'est pas à analyser
                        return false;
                    }
                }
            }

            //S'il y pas de règle ou que aucune règle ne correspond au nom du fichier, le fichier doit etre analysé
            return true;
        }
    }
}
